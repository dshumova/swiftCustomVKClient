//
//  MessageRealmModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 17.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class MessageRealmModel: Object
{
    @objc dynamic var id = 0
    @objc dynamic var date = 0
    @objc dynamic var body: String?
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
}
