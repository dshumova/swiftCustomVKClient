//
//  DialogMessagesRealmModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 17.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class DialogMessagesRealmModel: Object
{
    @objc dynamic var userID = 0
    let dialogMessages = List<MessageRealmModel>()
    
    override static func primaryKey() -> String?
    {
        return "userID"
    }
}
