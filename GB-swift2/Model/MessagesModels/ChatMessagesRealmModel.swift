//
//  ChatMessagesRealmModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 17.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class ChatMessagesRealmModel: Object
{
    @objc dynamic var chatID = 0
    let chatMessages = List<MessageRealmModel>()
    
    override static func primaryKey() -> String?
    {
        return "chatID"
    }
}
