//
//  AllMessagesRealmModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 17.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class AllMessagesRealmModel: Object
{
    let allChatMessages = List<ChatMessagesRealmModel>()
    let allDialogMessages = List<DialogMessagesRealmModel>()
}
