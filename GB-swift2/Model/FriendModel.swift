//
//  FriendModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 01.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class FriendModel: Object, Decodable
{
    @objc dynamic var id = 0
    @objc dynamic var first_name: String?
    @objc dynamic var last_name: String?
    @objc dynamic var photo_max: String?
    @objc dynamic var online = 0
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
}

class VKFriendsResponse: Decodable
{
    var items: [FriendModel]?
    
    enum CodingKeys: String, CodingKey
    {
        case response
    }
    
    enum ResponseCodingKeys: String, CodingKey
    {
        case items
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let response = try container.nestedContainer(keyedBy: ResponseCodingKeys.self, forKey: .response)
        items = try response.decode([FriendModel].self, forKey: .items)
    }
}
