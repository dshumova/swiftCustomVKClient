//
//  GroupModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 02.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class GroupModel: Object, Decodable
{
    @objc dynamic var id = 0
    @objc dynamic var name: String?
    @objc dynamic var members_count = 0
    @objc dynamic var photo_max: String?
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
}

class VKGroupsResponse: Decodable
{
    var items: [GroupModel]?
    
    enum CodingKeys: String, CodingKey
    {
        case response
    }
    
    enum ResponseCodingKeys: String, CodingKey
    {
        case items
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let response = try container.nestedContainer(keyedBy: ResponseCodingKeys.self, forKey: .response)
        items = try response.decode([GroupModel].self, forKey: .items)
    }
}

