//
//  LPServerModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 06.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class LPServerModel: Object, Decodable
{
    @objc dynamic var key: String?
    @objc dynamic var server: String?
    @objc dynamic var ts = 0
    @objc dynamic var pts = 0
    
    enum CodingKeys: String, CodingKey
    {
        case key
        case server
        case ts
        case pts
    }
}

class LPServerModelResponse: Decodable
{
    var response: LPServerModel?
}
