//
//  NewsFeedModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 09.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation

class NewsFeedModel: Decodable
{
    var profiles: [profile]?
    var groups: [group]?
    var next_from: String?
    var items: [NewsModel]?
    
    struct profile: Decodable
    {
        var id: Int?
        var name: String?
        {
            get
            {
                return "\(first_name ?? "") \(last_name ?? "")"
            }
        }
        var first_name: String?
        var last_name: String?
        var photo_100: String?
    }
    
    struct group: Decodable
    {
        var id: Int?
        var name: String?
        var photo_100: String?
    }
    
    enum CodingKeys: String, CodingKey
    {
        case response
    }
    
    enum ResponseCodingKeys: String, CodingKey
    {
        case items
        case profiles
        case groups
        case next_from
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let response = try container.nestedContainer(keyedBy: ResponseCodingKeys.self, forKey: .response)
        
        profiles = try response.decode([profile].self, forKey: .profiles)
        groups = try response.decode([group].self, forKey: .groups)
        next_from = try response.decode(String.self, forKey: .next_from)
        items = try response.decode([NewsModel].self, forKey: .items)
    }
    
    func getAuthorName(news: Int) -> String
    {
        if news < 0
        {
            let authorID = -news
            let currentAuthor = groups?.filter({$0.id == authorID})
            return currentAuthor?.first?.name ?? ""
        }
        else
        {
            let currentAuthor = profiles?.filter({$0.id == news})
            return currentAuthor?.first?.name ?? ""
        }
    }
    
    func getAutorPhoto(news: Int) -> String
    {
        if news < 0
        {
            let authorID = -news
            let currentAuthor = groups?.filter({$0.id == authorID})
            return currentAuthor?.first?.photo_100 ?? ""
        }
        else
        {
            let currentAuthor = profiles?.filter({$0.id == news})
            return currentAuthor?.first?.photo_100 ?? ""
        }
    }
}
