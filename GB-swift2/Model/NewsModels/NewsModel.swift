//
//  NewsModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 09.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation

class NewsModel: Decodable
{
    var source_id: Int?
    var text: String?
    var newsPhoto: String?
    var comments: Int?
    var likes: Int?
    var reposts: Int?
    
    struct copyHistory: Decodable
    {
        var attachments: [attach]?
        var text: String?
    }
    struct attach: Decodable
    {
        var link: linkStruct?
        var photo: photoStruct?
        var video: videoStruct?
    }
    struct photoStruct: Decodable
    {
        var photo_604: String?
    }
    struct linkStruct: Decodable
    {
        var photo: photoStruct?
    }
    struct videoStruct: Decodable
    {
        var photo_640: String?
    }
    
    enum CodingKeys: String, CodingKey
    {
        case source_id
        case text
        case attachments
        case comments
        case likes
        case reposts
        case copy_history
    }
    
    enum CommentsCodingKeys: String, CodingKey
    {
        case count
    }
    
    enum LikesCodingKeys: String, CodingKey
    {
        case count
    }
    
    enum RepostsCodingKeys: String, CodingKey
    {
        case count
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        source_id = try container.decode(Int.self, forKey: .source_id)
        
        let commentContainer = try container.nestedContainer(keyedBy: CommentsCodingKeys.self, forKey: .comments)
        comments = try commentContainer.decode(Int.self, forKey: .count)
        
        let likesContainer = try container.nestedContainer(keyedBy: LikesCodingKeys.self, forKey: .likes)
        likes = try likesContainer.decode(Int.self, forKey: .count)
        
        let repostsContainer = try container.nestedContainer(keyedBy: RepostsCodingKeys.self, forKey: .reposts)
        reposts = try repostsContainer.decode(Int.self, forKey: .count)
        
        if let photoArray = try container.decodeIfPresent([attach].self, forKey: .attachments)
        {
            text = try container.decode(String.self, forKey: .text)
            if let linkPhoto = photoArray.first?.link?.photo?.photo_604
            {
                newsPhoto = linkPhoto
            }
            else if let photoPhoto = photoArray.first?.photo?.photo_604
            {
                newsPhoto = photoPhoto
            }
            else
            {
                newsPhoto = photoArray.first?.video?.photo_640
            }
        }
        else if let historyCopyArray = try container.decodeIfPresent([copyHistory].self, forKey: .copy_history)
        {
            text = historyCopyArray.first?.text
            let photoArray = historyCopyArray.first?.attachments
            if let linkPhoto = photoArray?.first?.link?.photo?.photo_604
            {
                newsPhoto = linkPhoto
            }
            else if let photoPhoto = photoArray?.first?.photo?.photo_604
            {
                newsPhoto = photoPhoto
            }
            else
            {
                newsPhoto = photoArray?.first?.video?.photo_640
            }
        }
    }
}
