//
//  UserModel.swift
//  GB-swift2
//
//  Created by Daria Shumova on 16.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class UserModel: Object, Decodable
{
    @objc dynamic var access_token: String?
    @objc dynamic var user_id = 0
    
    let friends = List<FriendModel>()
    let groups = List<GroupModel>()
    
    enum CodingKeys: String, CodingKey
    {
        case access_token
        case user_id
    }

    override static func primaryKey() -> String?
    {
        return "user_id"
    }
}
