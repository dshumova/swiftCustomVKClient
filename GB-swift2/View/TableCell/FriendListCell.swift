//
//  FriendListCell.swift
//  GB-swift2
//
//  Created by Daria Shumova on 02.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class FriendListCell: UITableViewCell
{
    @IBOutlet weak var avatar: RoundImage!
    @IBOutlet weak var friendName: UILabel!
    @IBOutlet weak var isOnlineIndicator: RoundImage!
}
