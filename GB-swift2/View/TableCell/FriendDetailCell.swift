//
//  FriendDetailCell.swift
//  GB-swift2
//
//  Created by Daria Shumova on 02.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class FriendDetailCell: UICollectionViewCell
{
    @IBOutlet weak var avatar: UIImageView!
}
