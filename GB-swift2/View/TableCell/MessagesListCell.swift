//
//  MessagesListCell.swift
//  GB-swift2
//
//  Created by Daria Shumova on 16.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class MessagesListCell: UITableViewCell
{
    @IBOutlet weak var chatAvatar: RoundImage!
    @IBOutlet weak var chatName: UILabel!
    @IBOutlet weak var lastMessagePreview: UILabel!
}
