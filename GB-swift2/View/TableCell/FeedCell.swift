//
//  FeedCell.swift
//  GB-swift2
//
//  Created by Daria Shumova on 06.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell
{
    let minInsets: CGFloat = 15.0
    let maxInsets: CGFloat = 30.0
    
    @IBOutlet weak var containerAvatar: ShadowRoundView!
    {
        didSet
        {
            containerAvatar.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var authorAvatar: RoundImage!
    {
        didSet
        {
            authorAvatar.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var autor: UILabel!
    {
        didSet
        {
            autor.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var news: UITextView!
    {
        didSet
        {
            news.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var photoNews: UIImageView!
    {
        didSet
        {
            photoNews.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    @IBOutlet weak var like: UIImageView!
    {
        didSet
        {
            like.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var comment: UIImageView!
    {
        didSet
        {
            comment.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var quote: UIImageView!
    {
        didSet
        {
            quote.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    @IBOutlet weak var numberOfLikes: UILabel!
    {
        didSet
        {
            numberOfLikes.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var numberOfComments: UILabel!
    {
        didSet
        {
            numberOfComments.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var numberOfQuotes: UILabel!
    {
        didSet
        {
            numberOfQuotes.translatesAutoresizingMaskIntoConstraints = false
        }
    }
   
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        authorAvatarFrame()
        authorLabelFrame()
        
        newsFrame()
        
        likeImageFrame()
        likeLabelFrame()
        commentImageFrame()
        commentsLabelFrame()
        quoteImageFrame()
        quotesLabelFrame()
        
        photoNewsFrame()
    }
    
    func setAuthor(name: String)
    {
        autor.text = name
        authorLabelFrame()
    }
    
    func setNews(text: String)
    {
        news.text = text
        newsFrame()
    }
    
    func setnumberOf(likes: String, comments: String, quotes: String)
    {
        numberOfLikes.text = likes
        numberOfComments.text = comments
        numberOfQuotes.text = quotes
        likeLabelFrame()
        commentsLabelFrame()
        quotesLabelFrame()
    }
    
    func authorAvatarFrame()
    {
        let sideSize = 40
        let avatarSize = CGSize(width: sideSize, height: sideSize)
        let containerAvatarOrigin = CGPoint(x: minInsets, y: minInsets)
        containerAvatar.frame = CGRect(origin: containerAvatarOrigin, size: avatarSize)
        authorAvatar.frame = CGRect(origin: containerAvatar.bounds.origin, size: avatarSize)
    }
    
    func authorLabelFrame()
    {
        let authorNameSize = getTextBlockSize(text: autor.text!, font: autor.font)
        let authorLabelSize = CGSize(width: ceil(bounds.width - containerAvatar.frame.width - 3 * minInsets), height: authorNameSize.height)
        let authorX = containerAvatar.frame.maxX + minInsets
        let authorY = containerAvatar.frame.midY - authorLabelSize.height / 2
        let authorOrigin = CGPoint(x: authorX, y: authorY)
        
        autor.frame = CGRect(origin: authorOrigin, size: authorLabelSize)
    }
    
    func newsFrame()
    {
        let newsMaxHeight = ceil(bounds.height / 2 - 2 * minInsets - containerAvatar.frame.height)
        let newsTextSize = getTextBlockSize(text: news.text, font: news.font!)
        let newsHeight = newsMaxHeight < newsTextSize.height ? newsMaxHeight : newsTextSize.height
        let newsSize = CGSize(width: bounds.width - minInsets * 2, height: newsHeight)
        
        let newsX = minInsets
        let newsY = 2 * minInsets + containerAvatar.frame.height
        let newsOrigin = CGPoint(x: newsX, y: newsY)
        
        news.frame = CGRect(origin: newsOrigin, size: newsSize)
    }
    
    func photoNewsFrame()
    {
        let width = bounds.width - 2 * minInsets
        let height = bounds.height - containerAvatar.frame.height - news.frame.height - like.frame.height - 5 * minInsets
        let size = CGSize(width: width, height: height)
        
        let x = minInsets
        let y = news.frame.maxY + minInsets
        let origin = CGPoint(x: x, y: y)
        
        photoNews.frame = CGRect(origin: origin, size: size)
    }
    
    func likeImageFrame()
    {
        let sideSize = 20
        let likeSize = CGSize(width: sideSize, height: sideSize)
        let likeX = minInsets
        let likeY = bounds.height - minInsets - likeSize.height
        let likeOrigin = CGPoint(x: likeX, y: likeY)
        
        like.frame = CGRect(origin: likeOrigin, size: likeSize)
    }
    
    func commentImageFrame()
    {
        let sideSize = 20
        let commentSize = CGSize(width: sideSize, height: sideSize)
        let commentX = numberOfLikes.frame.maxX + maxInsets
        let commentY = like.frame.origin.y
        let commentOrigin = CGPoint(x: commentX, y: commentY)
        
        comment.frame = CGRect(origin: commentOrigin, size: commentSize)
    }
    
    func quoteImageFrame()
    {
        let sideSize = 20
        let quoteSize = CGSize(width: sideSize, height: sideSize)
        let quoteX = numberOfComments.frame.maxX + maxInsets
        let quoteY = like.frame.origin.y
        let quoteOrigin = CGPoint(x: quoteX, y: quoteY)
        
        quote.frame = CGRect(origin: quoteOrigin, size: quoteSize)
    }
    
    func likeLabelFrame()
    {
        let likeLabelSize = getTextBlockSize(text: numberOfLikes.text!, font: numberOfLikes.font)
        let likeLabelX = like.frame.maxX + minInsets
        let likeLabelY = like.frame.midY - likeLabelSize.height / 2
        let likeLabelOrigin = CGPoint(x: likeLabelX, y: likeLabelY)
        
        numberOfLikes.frame = CGRect(origin: likeLabelOrigin, size: likeLabelSize)
    }
    
    func commentsLabelFrame()
    {
        let commentsLabelSize = getTextBlockSize(text: numberOfComments.text!, font: numberOfComments.font)
        let commentsLabelX = comment.frame.maxX + minInsets
        let commentsLabelY = like.frame.midY - commentsLabelSize.height / 2
        let commentsLabelOrigin = CGPoint(x: commentsLabelX, y: commentsLabelY)
        
        numberOfComments.frame = CGRect(origin: commentsLabelOrigin, size: commentsLabelSize)
    }
    
    func quotesLabelFrame()
    {
        let quotesLabelSize = getTextBlockSize(text: numberOfQuotes.text!, font: numberOfQuotes.font)
        let quotesLabelX = quote.frame.maxX + minInsets
        let quotesLabelY = like.frame.midY - quotesLabelSize.height / 2
        let quotesLabelOrigin = CGPoint(x: quotesLabelX, y: quotesLabelY)
        
        numberOfQuotes.frame = CGRect(origin: quotesLabelOrigin, size: quotesLabelSize)
    }
    
    func getTextBlockSize(text: String, font: UIFont) -> CGSize
    {
        let maxWidth = bounds.width - minInsets * 2
        let textBlock = CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude)
        let rect = text.boundingRect(with: textBlock, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        let width = Double(rect.size.width)
        let height = Double(rect.size.height)
        
        return CGSize(width: ceil(width), height: ceil(height))
    }
}
