//
//  ShadowRoundView.swift
//  GB-swift2
//
//  Created by Daria Shumova on 01.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class ShadowRoundView: UIView
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        layer.cornerRadius = self.frame.size.width / 2
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize (width: 1.0, height: 1.0)
        layer.shadowRadius = 5.0
    }
}
