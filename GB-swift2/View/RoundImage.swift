//
//  RoundImage.swift
//  GB-swift2
//
//  Created by Daria Shumova on 02.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class RoundImage: UIImageView
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        layer.cornerRadius = self.frame.size.width / 2
        clipsToBounds = true
    }
}
