//
//  ViewExtension.swift
//  GB-swift2
//
//  Created by Daria Shumova on 22.02.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

extension UIView
{
    func setGradientBackground(firstColor: UIColor, secondColor:UIColor)
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
}
