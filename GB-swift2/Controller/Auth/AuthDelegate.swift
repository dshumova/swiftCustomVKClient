//
//  AuthDelegate.swift
//  GB-swift2
//
//  Created by Daria Shumova on 16.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation

protocol AuthDelegate: class
{
    func disappearSocialAuthorizationView()
    func showNextPage()
    func showAllert(_ message: String)
}
