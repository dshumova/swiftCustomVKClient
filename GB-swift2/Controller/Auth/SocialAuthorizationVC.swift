//
//  SocialAuthorizationVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 16.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit
import WebKit
import RealmSwift

class SocialAuthorizationVC: UIViewController, WKNavigationDelegate
{
    private var vkWebView : WKWebView!
    private let vkApi = VkApi()
    var delegate: AuthDelegate?
    var success = false
    
    deinit
    {
        if success
        {
            vkApi.getSessionDataForLongPoll()
            delegate?.showNextPage()
        }
        else
        {
            delegate?.showAllert("Ошибка авторизации")
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupViewController()
        setupViewModel()
    }

    //MARK: - Private methods

    private func setupViewController()
    {
        vkWebView = WKWebView(frame: self.view.frame)
        vkWebView.navigationDelegate = self
        self.view.addSubview(vkWebView)
    }

    private func setupViewModel()
    {
        if let url = vkApi.getAuthURL()
        {
            vkWebView.load(URLRequest(url: url))
        }
        else
        {
            showErrorAlert(message: "Невозможно открыть страницу авторизации")
        }
    }
    
    private func showErrorAlert(message: String)
    {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    //MARK: - WKNavigationDelegate methods

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error)
    {
        vkWebView.isHidden = true
        showErrorAlert(message: error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        guard let url = webView.url else
        {
            showErrorAlert(message: "Невозможно открыть страницу авторизации")
            return
        }
        let components = URLComponents(string: url.absoluteString)

        if let fragment = components?.fragment
        {
            if let range = fragment.range(of: "code=")
            {
                let code = String(fragment.suffix(from: range.upperBound))
                if !code.isEmpty
                {
                    let session = URLSession.shared
                    session.dataTask(with: vkApi.getRedirectAuthURL(code: code)!) { data, _, error in
                        
                        guard let data = data else
                        {
                            print("Get auth data error - \(error?.localizedDescription ?? "")")
                            return
                        }
                        do
                        {
                            if let userModel = try JSONDecoder().decode(UserModel?.self, from: data)
                            {
                                self.success = true
                                if !RealmApi.addUser(userModel)
                                {
                                    self.success = false
                                }
                            }
                        }
                        catch
                        {
                            print(error)
                        }
                            
                        self.delegate?.disappearSocialAuthorizationView()
                    }.resume()
                }
                return
            }
            self.delegate?.disappearSocialAuthorizationView()
        }
    }
}
