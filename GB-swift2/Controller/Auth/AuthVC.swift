//
//  AuthVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 05.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit
import RealmSwift
import WebKit

class AuthVC: UIViewController, AuthDelegate
{
    var vkApi = VkApi()
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func unwindToRootViewController(segue: UIStoryboardSegue)
    {
        let dataStore = WKWebsiteDataStore.default()
        dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { (records) in
            for record in records
            {
                if record.displayName.contains("vk.com")
                {
                    dataStore.removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), for: [record])
                    {
                        print("Deleted: " + record.displayName);
                    }
                }
            }
        }
        RealmApi.clearBase()
        vkApi = VkApi()
    }

    @IBAction func authBtn(_ sender: Any)
    {
        let viewController = SocialAuthorizationVC()
        viewController.delegate = self
        self.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        if let sublayersCount = view.layer.sublayers?.count
        {
            if sublayersCount > 2
            {
                view.layer.sublayers?.first?.removeFromSuperlayer()
            }
        }
        view.setGradientBackground(firstColor: NAVIGATION_BACKGROUND_COLOR, secondColor: TABBAR_BACKGROUND_COLOR)
    }
    
    // MARK: - AuthDelegate
    
    func disappearSocialAuthorizationView()
    {
        dismiss(animated: true)
    }
    
    func showNextPage()
    {
        performSegue(withIdentifier: "AuthSegue", sender: nil)
    }
    
    func showAllert(_ message: String)
    {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
