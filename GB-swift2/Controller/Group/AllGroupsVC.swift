//
//  AllGroupsVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 02.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class AllGroupsVC: UITableViewController, UISearchBarDelegate
{
    let vkAPI = VkApi()
    let fotoManager = FotoManager()
    var filteredGroups: [GroupModel]?
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.placeholder = "Найти группу"
        searchBar.barTintColor = NAVIGATION_BACKGROUND_COLOR
        
        tableView.backgroundColor = NAVIGATION_BACKGROUND_COLOR
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        tableView.rowHeight = 120
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return filteredGroups?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AllGroupsCell", for: indexPath) as? GroupCell
        {
            if filteredGroups != nil
            {
                fotoManager.getFoto(urlStr: filteredGroups![indexPath.row].photo_max!) { someImage in
                    DispatchQueue.main.async
                    {
                        if let newIndexPath = tableView.indexPath(for: cell), newIndexPath == indexPath || tableView.indexPath(for: cell) == nil
                        {
                            cell.avatar.image = someImage
                        }
                    }
                }
                cell.groupName.text = filteredGroups![indexPath.row].name
                cell.groupMemberCount.text = "\(filteredGroups![indexPath.row].members_count) участников"
                return cell
            }
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.01
    }
    
    // MARK: - SearchBar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if !searchText.isEmpty
        {
            vkAPI.searchByGroup(searchText: searchText, viewControllerNeedToUpdate: self)
        }
        else
        {
            filteredGroups = nil
        }
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
    }
}
