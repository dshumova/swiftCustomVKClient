//
//  GroupListVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 02.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit
import RealmSwift

class GroupListVC: PageVC, UISearchBarDelegate
{
    let vkAPI = VkApi()
    let fotoManager = FotoManager()
    var realmNotificationToken: NotificationToken?
    var groups: Results<GroupModel>?
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBAction func addGroup(segue: UIStoryboardSegue)
    {
        if segue.identifier == "AddGroupSegue"
        {
            if let allGroupVC = segue.source as? AllGroupsVC
            {
                if let index = allGroupVC.tableView.indexPathForSelectedRow?.row
                {
                    let newGroup = allGroupVC.filteredGroups![index]
                    vkAPI.joinGroupAndUpdateUI(group: newGroup, viewControllerNeedToUpdate: self)
                }
            }
        }
    }
    
    lazy var realmNotificationBlock: (RealmCollectionChange<Results<GroupModel>>) -> Void = { [weak self] changes in
        guard let weakSelf = self else
        {
            return
        }
        switch changes
        {
        case .initial:
            weakSelf.tableView.reloadData()
        case .update(_, let deletions, let insertions, let modifications):
            weakSelf.tableView.beginUpdates()
            weakSelf.tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }), with: .automatic)
            weakSelf.tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}), with: .automatic)
            weakSelf.tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }), with: .automatic)
            weakSelf.tableView.endUpdates()
        case .error(let error):
            fatalError("\(error)")
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.placeholder = "Найти группу"
        searchBar.barTintColor = NAVIGATION_BACKGROUND_COLOR
        
        getRealmToken()
        
        vkAPI.getUserGroup()
        
        vkAPI.updateLPConnection()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let searcnBarHeight = searchBar.intrinsicContentSize.height
        tableView.setContentOffset(CGPoint.init(x: 0, y: searcnBarHeight), animated: false)
        
        tableView.rowHeight = 120
    }
    
    private func getRealmToken()
    {
        groups = RealmApi.getGroups()
        realmNotificationToken = groups?.observe(realmNotificationBlock)
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return groups?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MyGroupsCell", for: indexPath) as? GroupCell
        {
            let name = groups?[indexPath.row].name ?? ""
            let membersCount = groups?[indexPath.row].members_count ?? 0
            if let fotoURLStr = groups?[indexPath.row].photo_max
            {
                self.fotoManager.getFoto(urlStr: fotoURLStr) { someImage in
                    DispatchQueue.main.async
                    {
                        if let newIndexPath = tableView.indexPath(for: cell), newIndexPath == indexPath || tableView.indexPath(for: cell) == nil
                        {
                            cell.avatar.image = someImage
                        }
                    }
                }
            }
            cell.groupName.text = name
            cell.groupMemberCount.text = "\(membersCount) участников"
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.delete
        {
            if let group = groups?[indexPath.row]
            {
                vkAPI.leaveGroupAndUpdateUI(group: group, viewControllerNeedToUpdate: self)
            }
        }
    }
    
    // MARK: - SearchBar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        realmNotificationToken?.invalidate()
        if !searchText.isEmpty
        {
            groups = RealmApi.filterGroups(searchText: searchText)
        }
        else
        {
            groups = RealmApi.getGroups()
        }
        realmNotificationToken = groups?.observe(realmNotificationBlock)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
    }
    
    func showAlert(message: String)
    {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.default, handler: nil))
        DispatchQueue.main.async
        {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    deinit
    {
        realmNotificationToken?.invalidate()
    }
}
