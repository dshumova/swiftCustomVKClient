//
//  ViewController.swift
//  GB-swift2
//
//  Created by Daria Shumova on 21.02.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate
{
    let vkAPI = VkApi()
    
    let firstBackgroundColor = UIColor(red: 6/255, green: 1/255, blue: 47/255, alpha: 1)
    let secondBackgroundColor = UIColor(red: 52/255, green: 24/255, blue: 63/255, alpha: 1)
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBAction func enterBtn(_ sender: Any)
    {
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        loginField.delegate = self
        passwordField.delegate = self
        
        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        scroll.addGestureRecognizer(hideKeyboardGesture)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        if let sublayersCount = view.layer.sublayers?.count
        {
            if sublayersCount > 1
            {
                view.layer.sublayers?.first?.removeFromSuperlayer()
            }
        }
        view.setGradientBackground(firstColor: firstBackgroundColor, secondColor: secondBackgroundColor)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func hideKeyboard()
    {
        scroll.endEditing(true)
    }
 
    @objc func keyboardWillShow(notification: Notification)
    {
        let info = notification.userInfo! as NSDictionary
        let kbSize = (info.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height/3, 0.0)
        
        scroll.contentInset = contentInsets
        scroll.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: Notification)
    {
        let contentInsets = UIEdgeInsets.zero
        scroll.contentInset = contentInsets
        scroll.scrollIndicatorInsets = contentInsets
    }
    
    // MARK: - TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == loginField
        {
            passwordField.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return true
    }
}

