//
//  FeedVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 06.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class FeedVC: PageVC
{
    let vkAPI = VkApi()
    let fotoManager = FotoManager()
    var feed: NewsFeedModel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupRefreshControl()
        
        vkAPI.getNewsFeed(viewControllerNeedToUpdate: self)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        tableView.rowHeight = view.bounds.height / 1.5
    }
    
    func setupRefreshControl()
    {
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = .white
        let attribute = [NSAttributedStringKey.foregroundColor: UIColor.white]
        refreshControl?.attributedTitle = NSAttributedString(string: "Обновляем ленту", attributes: attribute)
        refreshControl?.addTarget(self, action: #selector(reloadFeed), for: .valueChanged)
    }
    
    @objc func reloadFeed()
    {
        feed = nil
        vkAPI.getNewsFeed(viewControllerNeedToUpdate: self)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return feed?.items?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as? FeedCell
        {
            if let currentNews = feed?.items![indexPath.row]
            {
                if let authorPhotoUrl = feed?.getAutorPhoto(news: currentNews.source_id!)
                {
                    self.fotoManager.getFoto(urlStr: authorPhotoUrl) { someImage in
                        DispatchQueue.main.async
                        {
                            if let newIndexPath = tableView.indexPath(for: cell), newIndexPath == indexPath || tableView.indexPath(for: cell) == nil
                            {
                                cell.authorAvatar.image = someImage
                            }
                        }
                    }
                }
                cell.setAuthor(name: feed?.getAuthorName(news: currentNews.source_id!) ?? "")
                cell.setNews(text: currentNews.text ?? "")
                cell.setnumberOf(likes: String(currentNews.likes ?? 0), comments: String(currentNews.comments ?? 0), quotes: String(currentNews.reposts ?? 0))
                
                if let newsPhotoUrl = currentNews.newsPhoto
                {
                    self.fotoManager.getFoto(urlStr: newsPhotoUrl) { someImage in
                        DispatchQueue.main.async
                        {
                            if let newIndexPath = tableView.indexPath(for: cell), newIndexPath == indexPath || tableView.indexPath(for: cell) == nil
                            {
                                cell.photoNews.image = someImage
                            }
                        }
                    }
                }
                else
                {
                    cell.photoNews.image = #imageLiteral(resourceName: "stub")
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let offsetY = scrollView.contentOffset.y
        let scrollHeight = scrollView.frame.size.height
        
        let endScrolling = offsetY + scrollHeight
        
        if endScrolling >= scrollView.contentSize.height * 0.5
        {
            vkAPI.getNewsFeed(startFrom: (feed?.next_from)!, viewControllerNeedToUpdate: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let indexPath = tableView.indexPathForSelectedRow
        {
            let destinationVC = segue.destination as! NewsDetailVC
            let cell = tableView.cellForRow(at: indexPath) as! FeedCell
            destinationVC.photo = cell.photoNews.image
        }
    }
}
