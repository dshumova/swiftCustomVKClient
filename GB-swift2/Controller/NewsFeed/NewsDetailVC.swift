//
//  NewsDetailVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 10.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class NewsDetailVC: UIViewController
{
    var photo: UIImage?
    
    @IBOutlet weak var detailPhoto: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        view.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        
        if photo != nil
        {
            detailPhoto.image = photo
        }
    }
}
