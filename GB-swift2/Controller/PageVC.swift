//
//  PageVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 06.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class PageVC: UITableViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()

        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = NAVIGATION_BACKGROUND_COLOR
        navigationController?.navigationBar.barStyle = .black
        
        tabBarController?.tabBar.barTintColor = TABBAR_BACKGROUND_COLOR
        tabBarController?.tabBar.isTranslucent = false
        
        tableView.backgroundColor = NAVIGATION_BACKGROUND_COLOR
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.01
    }
}
