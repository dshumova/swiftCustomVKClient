//
//  FriendDetailVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 02.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class FriendDetailVC: UICollectionViewController
{
    var passedValue = UIImage()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }

    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendDetailCell", for: indexPath) as? FriendDetailCell
        {
            cell.avatar.image = passedValue
            return cell
        }
        return UICollectionViewCell()
    }
}
