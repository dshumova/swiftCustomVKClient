//
//  FriendListVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 01.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit
import RealmSwift

class FriendListVC: PageVC, UISearchBarDelegate
{
    let vkAPI = VkApi()
    let fotoManager = FotoManager()
    var realmNotificationToken: NotificationToken?
    var friends: Results<FriendModel>?
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    lazy var realmNotificationBlock: (RealmCollectionChange<Results<FriendModel>>) -> Void = { [weak self] changes in
        guard let weakSelf = self else
        {
            return
        }
        switch changes
        {
        case .initial:
            weakSelf.tableView.reloadData()
        case .update(_, let deletions, let insertions, let modifications):
            weakSelf.tableView.beginUpdates()
            weakSelf.tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }), with: .automatic)
            weakSelf.tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}), with: .automatic)
            weakSelf.tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }), with: .automatic)
            weakSelf.tableView.endUpdates()
        case .error(let error):
            fatalError("\(error)")
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.placeholder = "Найти друга"
        searchBar.barTintColor = NAVIGATION_BACKGROUND_COLOR
        
        getRealmToken()

        vkAPI.getFriendList()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let searcnBarHeight = searchBar.intrinsicContentSize.height
        tableView.setContentOffset(CGPoint.init(x: 0, y: searcnBarHeight), animated: false)

        tableView.rowHeight = 120
    }
    
    private func getRealmToken()
    {
        friends = RealmApi.getFriends()
        realmNotificationToken = friends?.observe(realmNotificationBlock)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return friends?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCell", for: indexPath) as? FriendListCell
        {
            let name = (friends?[indexPath.row].first_name ?? "") + " " + (friends?[indexPath.row].last_name ?? "")
            if let fotoURLStr = friends?[indexPath.row].photo_max
            {
                self.fotoManager.getFoto(urlStr: fotoURLStr) { someImage in
                    DispatchQueue.main.async
                    {
                        if let newIndexPath = tableView.indexPath(for: cell), newIndexPath == indexPath || tableView.indexPath(for: cell) == nil
                        {
                            cell.avatar.image = someImage
                        }
                    }
                }
            }
            cell.friendName.text = name
            if friends?[indexPath.row].online == 1
            {
                cell.isOnlineIndicator.backgroundColor = .green
            }
            else if friends?[indexPath.row].online == 0
            {
                cell.isOnlineIndicator.backgroundColor = .red
            }
            return cell
        }
        return UITableViewCell()
    }
    
     // MARK: - Navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?)
     {
        if segue.identifier == "FriendDetailSegue"
        {
            if let indexPath = tableView.indexPathForSelectedRow
            {
                let destinationVC = segue.destination as! FriendDetailVC
                let cell = tableView.cellForRow(at: indexPath) as! FriendListCell
                destinationVC.passedValue = cell.avatar.image!
            }
        }
     }
    
    // MARK: - SearchBar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        realmNotificationToken?.invalidate()
        if !searchText.isEmpty
        {
            friends = RealmApi.filterFriends(searchText: searchText)
        }
        else
        {
            friends = RealmApi.getFriends()
        }
        realmNotificationToken = friends?.observe(realmNotificationBlock)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
    }
    
    deinit
    {
        realmNotificationToken?.invalidate()
    }
}
