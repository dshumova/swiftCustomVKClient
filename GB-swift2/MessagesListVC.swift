//
//  MessagesListVC.swift
//  GB-swift2
//
//  Created by Daria Shumova on 16.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit
import RealmSwift

class MessagesListVC: PageVC
{
    let vkAPI = VkApi()
    let fotoManager = FotoManager()
    var realmNotificationToken: NotificationToken?
    var messages: Results<AllMessagesRealmModel>?
    var friends: Results<FriendModel>?
    
    lazy var realmNotificationBlock: (RealmCollectionChange<Results<AllMessagesRealmModel>>) -> Void = { [weak self] changes in
        guard let weakSelf = self else
        {
            return
        }
        switch changes
        {
        case .initial:
            weakSelf.tableView.reloadData()
        case .update(_, let deletions, let insertions, let modifications):
            weakSelf.tableView.beginUpdates()
            weakSelf.tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }), with: .automatic)
            weakSelf.tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}), with: .automatic)
            weakSelf.tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }), with: .automatic)
            weakSelf.tableView.endUpdates()
        case .error(let error):
            fatalError("\(error)")
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        getRealmToken()
        
        vkAPI.getFriendList()
        
        friends = RealmApi.getFriends()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        tableView.rowHeight = 80
    }
    
    private func getRealmToken()
    {
        messages = RealmApi.getAllMessages()
        realmNotificationToken = messages?.observe(realmNotificationBlock)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (messages?.first?.allChatMessages.count ?? 0) + (messages?.first?.allDialogMessages.count ?? 0)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MessageListCell", for: indexPath) as? MessagesListCell
        {
            let messagesStruct = messages?.first
            let dialogCount = messagesStruct?.allDialogMessages.count
            if dialogCount! > indexPath.row
            {
                if let index = friends?.index(where: { $0.id == messagesStruct?.allDialogMessages[indexPath.row].userID })
                {
                    let friend = friends![index]
                    let name = (friend.first_name ?? "") + " " + (friend.last_name ?? "")
                    cell.chatName.text = name
                    print("NAME: \(name)")
                    
                    if let fotoURLStr = friend.photo_max
                    {
                        self.fotoManager.getFoto(urlStr: fotoURLStr) { someImage in
                            DispatchQueue.main.async
                            {
                                if let newIndexPath = tableView.indexPath(for: cell), newIndexPath == indexPath || tableView.indexPath(for: cell) == nil
                                {
                                    cell.chatAvatar.image = someImage
                                }
                            }
                        }
                    }
                }
                cell.lastMessagePreview.text = messagesStruct?.allDialogMessages[indexPath.row].dialogMessages.last?.body
            }
            else
            {
                cell.lastMessagePreview.text = messagesStruct?.allChatMessages[indexPath.row - dialogCount!].chatMessages.last?.body
            }
            return cell
        }
        return UITableViewCell()
    }
    
    deinit
    {
        realmNotificationToken?.invalidate()
    }
}
