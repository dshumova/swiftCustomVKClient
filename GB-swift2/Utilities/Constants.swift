//
//  Constants.swift
//  GB-swift2
//
//  Created by Daria Shumova on 06.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import UIKit

public let NAVIGATION_BACKGROUND_COLOR = UIColor(red: 6/255, green: 1/255, blue: 47/255, alpha: 1)
public let TABBAR_BACKGROUND_COLOR = UIColor(red: 52/255, green: 24/255, blue: 63/255, alpha: 1)

enum VKBackendError: Error
{
    case urlError(reason: String)
    case dataLoadFail(reason: String)
}

public enum Result<Value>
{
    case success(Value)
    case failure(Error)
    
    /// Returns the associated value if the result is a success, `nil` otherwise.
    public var value: Value?
    {
        switch self
        {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    /// Returns the associated error value if the result is a failure, `nil` otherwise.
    public var error: Error?
    {
        switch self
        {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}
