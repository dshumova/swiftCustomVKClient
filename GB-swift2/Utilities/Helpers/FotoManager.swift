//
//  Manager.swift
//  GB-swift2
//
//  Created by Daria Shumova on 18.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import UIKit

class FotoManager
{
    private let imageCache = NSCache<NSString, UIImage>()
    
    func getFoto(urlStr: String, completion: @escaping (UIImage?) -> Void)
    {
        if let cachedImage = imageCache.object(forKey: urlStr as NSString)
        {
            completion(cachedImage)
        }
        else
        {
            guard let url = URL(string: urlStr) else
            {
                completion(nil)
                return
            }
            let session = URLSession.shared
            session.dataTask(with: url) { data, _, error in
                if (error != nil)
                {
                    print(error?.localizedDescription ?? "")
                    completion(nil)
                }
                else if let data = data, let image = UIImage(data: data)
                {
                    self.imageCache.setObject(image, forKey: urlStr as NSString)
                    completion(image)
                }
                else
                {
                    completion(nil)
                }
            }.resume()
        }
    }
}
