//
//  ParseMessageHistory.swift
//  GB-swift2
//
//  Created by Daria Shumova on 16.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation

class ParseMessageHistory: Decodable
{
    struct rawItem: Decodable
    {
        var user_id: Int?
        var chat_id: Int?
        var id: Int?
        var date: Int?
        var body: String?
    }
    
    enum CodingKeys: String, CodingKey
    {
        case response
    }
    
    enum ResponseCodingKeys: String, CodingKey
    {
        case items
        case count
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let response = try container.nestedContainer(keyedBy: ResponseCodingKeys.self, forKey: .response)
        
        let items = try response.decode([rawItem].self, forKey: .items)
        
        var groupChats: [ChatMessagesRealmModel] = []
        var dialogChats: [DialogMessagesRealmModel] = []
        
        for item in items
        {
            let newMessage = MessageRealmModel()
            newMessage.id = item.id!
            newMessage.date = item.date!
            newMessage.body = item.body
            
            if item.chat_id != nil
            {
                if let chatIndex = groupChats.index(where: { $0.chatID == item.chat_id })
                {
                    groupChats[chatIndex].chatMessages.append(newMessage)
                }
                else
                {
                    let newChatMessage = ChatMessagesRealmModel()
                    newChatMessage.chatID = item.chat_id!
                    newChatMessage.chatMessages.append(newMessage)
                    groupChats.append(newChatMessage)
                }
            }
            else
            {
                if let dialogIndex = dialogChats.index(where: { $0.userID == item.user_id })
                {
                    dialogChats[dialogIndex].dialogMessages.append(newMessage)
                }
                else
                {
                    let newDialogMessage = DialogMessagesRealmModel()
                    newDialogMessage.userID = item.user_id!
                    newDialogMessage.dialogMessages.append(newMessage)
                    dialogChats.append(newDialogMessage)
                }
            }
        }
        let allMessages = AllMessagesRealmModel()
        allMessages.allChatMessages.append(objectsIn: groupChats)
        allMessages.allDialogMessages.append(objectsIn: dialogChats)
        RealmApi.addAllMessages(messageList: allMessages)
    }
}
