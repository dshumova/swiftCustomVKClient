//
//  RealmApi.swift
//  GB-swift2
//
//  Created by Daria Shumova on 16.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class RealmApi
{
    class func clearBase()
    {
        let realm = getRealmInstance()
        do
        {
            try realm?.write
            {
                realm?.deleteAll()
            }
        }
        catch
        {
            print(error)
        }
    }
    
    class func addUser(_ object: Object) -> Bool
    {
        if let realm = RealmApi.getRealmInstance()
        {
            do
            {
                try realm.write
                {
                    realm.add(object, update: true)
                }
                return true
            }
            catch
            {
                print(error)
            }
        }
        return false
    }
    
    // MARK: - Messages Methods
    
    class func getUser(forPrimaryKey: Int) -> Results<UserModel>?
    {
        let realm = getRealmInstance()
        return realm?.objects(UserModel.self).filter("user_id = %@", forPrimaryKey) ?? nil
    }
    
    class func addAllMessages(messageList: AllMessagesRealmModel)
    {
        if let realm = getRealmInstance()
        {
            do
            {
                try realm.write
                {
                    realm.delete(realm.objects(AllMessagesRealmModel.self))
                    realm.add(messageList)
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    class func getAllMessages() -> Results<AllMessagesRealmModel>?
    {
        let realm = getRealmInstance()
        return realm?.objects(AllMessagesRealmModel.self)
    }
    
    // MARK: - LPSession Methods
    
    class func addLPSessionData(_ lpData: LPServerModel)
    {
        if let realm = getRealmInstance()
        {
            do
            {
                try realm.write
                {
                    if let currentLP = getLPSessionData()
                    {
                        currentLP.first?.ts = lpData.ts
                        currentLP.first?.pts = lpData.pts
                    }
                    else
                    {
                        realm.add(lpData)
                    }
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    class func getLPSessionData() -> Results<LPServerModel>?
    {
        let realm = getRealmInstance()
        return realm?.objects(LPServerModel.self)
    }
    
    // MARK: - Friends Methods
    
    class func getFriends() -> Results<FriendModel>?
    {
        let realm = getRealmInstance()
        return realm?.objects(FriendModel.self) ?? nil
    }
    
    class func addFriends(friends: [FriendModel], userPrimaryKey: Int?)
    {
        if let realm = RealmApi.getRealmInstance()
        {
            if let user = realm.object(ofType: UserModel.self, forPrimaryKey: userPrimaryKey)
            {
                do
                {
                    try realm.write
                    {
                        realm.delete(user.friends)
                        user.friends.append(objectsIn: friends)
                    }
                }
                catch
                {
                    print(error)
                }
            }
        }
    }
    
    class func filterFriends(searchText: String) -> Results<FriendModel>?
    {
        guard let realm = RealmApi.getRealmInstance() else
        {
            return nil
        }
        let predicate = NSPredicate(format: "first_name CONTAINS [cd]%@ || last_name CONTAINS [cd]%@", searchText, searchText)
        let friends = realm.objects(FriendModel.self).filter(predicate)
            
        return friends
    }
    
    // MARK: - Groups Methods
    
    class func addGroups(groups: [GroupModel], userPrimaryKey: Int?)
    {
        if let realm = RealmApi.getRealmInstance()
        {
            if let user = realm.object(ofType: UserModel.self, forPrimaryKey: userPrimaryKey)
            {
                do
                {
                    try realm.write
                    {
                        realm.delete(user.groups)
                        user.groups.append(objectsIn: groups)
                    }
                }
                catch
                {
                    print(error)
                }
            }
        }
    }
    
    class func addGroup(group: GroupModel)
    {
        if let realm = RealmApi.getRealmInstance()
        {
            let userPrimaryKey = VkApi().getUserId()
            let user = realm.object(ofType: UserModel.self, forPrimaryKey: userPrimaryKey)
            do
            {
                try realm.write
                {
                    realm.add(group, update: true)
                    user?.groups.append(group)
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    class func getGroups() -> Results<GroupModel>?
    {
        let realm = getRealmInstance()
        return realm?.objects(GroupModel.self) ?? nil
    }
    
    class func deleteGroup(group: GroupModel)
    {
        if let realm = RealmApi.getRealmInstance()
        {
            do
            {
                try realm.write
                {
                    realm.delete(group)
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    class func filterGroups(searchText: String) -> Results<GroupModel>?
    {
        guard let realm = RealmApi.getRealmInstance() else
        {
            return nil
        }
        let predicate = NSPredicate(format: "name CONTAINS [cd]%@", searchText)
        let groups = realm.objects(GroupModel.self).filter(predicate)
            
        return groups
    }
    
    // MARK: - Private Methods
    
    private class func getRealmInstance() -> Realm?
    {
        var realm: Realm?
        do
        {
            let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
            realm = try Realm(configuration: config)
            print(realm?.configuration.fileURL ?? "")
        }
        catch
        {
            print(error)
        }
        return realm
    }
}
