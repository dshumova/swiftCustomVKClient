//
//  UpdateUI.swift
//  GB-swift2
//
//  Created by Daria Shumova on 14.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit

class UpdateUI: Operation
{
    enum uiType
    {
        case tableView
        case alertAdd
        case alertDelete
    }
    var ui: uiType
    weak var targetVC: UIViewController?
    var groupForActionsWGroups: GroupModel?
    
    init(uiType: uiType, targetVC: UIViewController, groupForActionsWGroups: GroupModel? = nil)
    {
        self.ui = uiType
        self.targetVC = targetVC
        self.groupForActionsWGroups = groupForActionsWGroups
    }
    
    override func main()
    {
        guard let parseData = dependencies.first as? ParseAndSaveData else
        {
            return
        }
        switch ui
        {
        case .tableView:
            if let newsFeedVC = targetVC as? FeedVC, let nextNews = parseData.outputData as? NewsFeedModel
            {
                if newsFeedVC.feed != nil
                {
                    newsFeedVC.feed?.items?.append(contentsOf: nextNews.items!)
                    newsFeedVC.feed?.groups?.append(contentsOf: nextNews.groups!)
                    newsFeedVC.feed?.profiles?.append(contentsOf: nextNews.profiles!)
                    newsFeedVC.feed?.next_from = nextNews.next_from
                }
                else
                {
                    newsFeedVC.feed = nextNews
                    newsFeedVC.refreshControl?.endRefreshing()
                }
                newsFeedVC.tableView.reloadData()
            }
            if let allGroupVC = targetVC as? AllGroupsVC
            {
                allGroupVC.filteredGroups = parseData.outputData as? [GroupModel]
                allGroupVC.tableView.reloadData()
            }
        case .alertAdd:
            if let groupVC = targetVC as? GroupListVC, let group = groupForActionsWGroups
            {
                if let response = parseData.outputData as? Bool
                {
                    if response
                    {
                        RealmApi.addGroup(group: group)
                    }
                    else
                    {
                        groupVC.showAlert(message: "Не удалось вступить в группу")
                    }
                }
            }
        case .alertDelete:
            if let groupVC = targetVC as? GroupListVC, let group = groupForActionsWGroups
            {
                if let response = parseData.outputData as? Bool
                {
                    if response
                    {
                        RealmApi.deleteGroup(group: group)
                    }
                    else
                    {
                        groupVC.showAlert(message: "Не удалось выйти из группы")
                    }
                }
            }
        }
    }
}
