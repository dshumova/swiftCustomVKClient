//
//  ParseAndSaveData.swift
//  GB-swift2
//
//  Created by Daria Shumova on 13.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit
import RealmSwift

class ParseAndSaveData: Operation
{
    enum vkObjectType
    {
        case friend
        case getGroup
        case groupReorganize
        case searchGroup
        case newsFeed
        case lpServerData
        case updateLPData
        case getMessageHistory
    }
    
    var parseType: vkObjectType
    var userID: Int
    var outputData: Any?
    
    init(parseType: vkObjectType, userID: Int)
    {
        self.parseType = parseType
        self.userID = userID
    }
    
    convenience init(parseType: vkObjectType)
    {
        self.init(parseType: parseType, userID: 0)
    }
    
    override func main()
    {
        guard let getDataOperation = dependencies.first as? GetDataOperation, let data = getDataOperation.data else
        {
            return
        }
        do
        {
            switch parseType
            {
            case .friend:
                let friends = try JSONDecoder().decode(VKFriendsResponse.self, from: data)
                if friends.items != nil
                {
                    RealmApi.addFriends(friends: friends.items!, userPrimaryKey: self.userID)
                }
                
            case .getGroup:
                let groups = try JSONDecoder().decode(VKGroupsResponse.self, from: data)
                if groups.items != nil
                {
                    RealmApi.addGroups(groups: groups.items!, userPrimaryKey: self.userID)
                }
                
            case .searchGroup:
                let groups = try JSONDecoder().decode(VKGroupsResponse.self, from: data)
                outputData = groups.items
                
            case .groupReorganize:
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                outputData = json["response"] as! Bool
                
            case .newsFeed:
                outputData = try JSONDecoder().decode(NewsFeedModel.self, from: data)
                
            case .lpServerData:
                let sessionData = try JSONDecoder().decode(LPServerModelResponse.self, from: data)
                RealmApi.addLPSessionData(sessionData.response!)
                
            case .updateLPData:
                let newSessionData = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                print("LPSERVER RESPONSE: \(newSessionData)")
                
            case .getMessageHistory:
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                print("MESSAGES HISTORY: \(json)")
                
                let jsonDecoder = JSONDecoder()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                jsonDecoder.dateDecodingStrategy = .formatted(dateFormatter)
                
                let _ = try jsonDecoder.decode(ParseMessageHistory.self, from: data)
            }
        }
        catch
        {
            print(error)
        }
    }
}
