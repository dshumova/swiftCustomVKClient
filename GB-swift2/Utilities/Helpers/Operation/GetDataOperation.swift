//
//  GetDataOperation.swift
//  GB-swift2
//
//  Created by Daria Shumova on 13.04.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import UIKit
import Foundation

class GetDataOperation: AsyncOperation​
{
    private var request: URLSessionDataTask?
    var url: URL?
    var data: Data?
    
    init(urlStr: String)
    {
        url = URL(string: urlStr)
    }
    
    override func cancel()
    {
        if request != nil
        {
            request!.cancel()
            super.cancel()
        }
    }
    
    override func main()
    {
        guard (url != nil) else
        {
            return
        }
        request = URLSession.shared.dataTask(with: url!) { [weak self] (data, _, error) in
            guard let data = data else
            {
                print(error as Any)
                return
            }
            self?.data = data
            self?.state = .finished
        }
        request?.resume()
    }
}
