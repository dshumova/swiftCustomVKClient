//
//  VkApi.swift
//  GB-swift2
//
//  Created by Daria Shumova on 05.03.2018.
//  Copyright © 2018 Daria Shumova. All rights reserved.
//

import Foundation
import RealmSwift

class VkApi
{
    private let CLIENT_ID = 6397421
    private let CLIENT_SECRET = "mL0BU7IUOrVwGeDd4SsM"
    private let BASE_URL = "https://api.vk.com/method/"
    private let AUTH_ENDPOINT = "https://oauth.vk.com/"
    private let REDIRECT_URI = "https://oauth.vk.com/blank.html"
    private let API_VERSION = 5.73
    private let LONG_POLL_VERSION = 3
    
    private var ACCESS_TOKEN: String?
    private var USER_ID: Int?
    
    init()
    {
        do
        {
            let realm = try Realm()
            ACCESS_TOKEN = realm.objects(UserModel.self).first?.access_token
            USER_ID = realm.objects(UserModel.self).first?.user_id
        }
        catch
        {
            print(error)
        }
    }
    
    func getToken() -> String?
    {
        if (ACCESS_TOKEN == nil)
        {
            do
            {
                let realm = try Realm()
                ACCESS_TOKEN = realm.objects(UserModel.self).first?.access_token
            }
            catch
            {
                print(error)
            }
        }
        return ACCESS_TOKEN
    }
    
    func getUserId() -> Int?
    {
        return USER_ID
    }
    
    func getAuthURL() -> URL?
    {
        return URL(string: "\(AUTH_ENDPOINT)authorize?client_id=\(CLIENT_ID)&display=mobile&redirect_uri=\(REDIRECT_URI)&scope=offline,groups,messages,wall,friends&response_type=code&revoke=1&v=\(API_VERSION)")
    }
    
    func getRedirectAuthURL(code: String) -> URL?
    {
        return URL(string: "\(AUTH_ENDPOINT)access_token?client_id=\(CLIENT_ID)&client_secret=\(CLIENT_SECRET)&redirect_uri=\(REDIRECT_URI)&code=\(code)")
    }
    
    func getFriendList()
    {
        if USER_ID == nil
        {
            do
            {
                let realm = try Realm()
                USER_ID = realm.objects(UserModel.self).first?.user_id
            }
            catch
            {
                print(error)
            }
        }
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)friends.get?user_id=\(USER_ID!)&fields=photo_max,online&v=\(API_VERSION)")
        queue.addOperation(getDataOperation)
        
        let parseAndSaveOperation = ParseAndSaveData(parseType: .friend, userID: USER_ID!)
        parseAndSaveOperation.addDependency(getDataOperation)
        queue.addOperation(parseAndSaveOperation)
    }
    
    func getUserGroup()
    {
        if USER_ID == nil
        {
            do
            {
                let realm = try Realm()
                USER_ID = realm.objects(UserModel.self).first?.user_id
            }
            catch
            {
                print(error)
            }
        }
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)groups.get?user_id=\(USER_ID!)&count=1000&extended=1&fields=members_count,name,photo_max&access_token=\(ACCESS_TOKEN!)&v=\(API_VERSION)")
        queue.addOperation(getDataOperation)
        
        let parseAndSaveOperation = ParseAndSaveData(parseType: .getGroup, userID: USER_ID!)
        parseAndSaveOperation.addDependency(getDataOperation)
        queue.addOperation(parseAndSaveOperation)
    }
    
    func searchByGroup(searchText: String, viewControllerNeedToUpdate: UIViewController)
    {
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)groups.search?q=\(searchText)&fields=photo_max,members_count&access_token=\(ACCESS_TOKEN!)&v=\(API_VERSION)")
        queue.addOperation(getDataOperation)
        
        let parseData = ParseAndSaveData(parseType: .searchGroup)
        parseData.addDependency(getDataOperation)
        queue.addOperation(parseData)
        
        let updateUI = UpdateUI(uiType: .tableView, targetVC: viewControllerNeedToUpdate)
        updateUI.addDependency(parseData)
        OperationQueue.main.addOperation(updateUI)
    }
    
    func joinGroupAndUpdateUI(group: GroupModel, viewControllerNeedToUpdate: UIViewController)
    {
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)groups.join?group_id=\(group.id)&access_token=\(ACCESS_TOKEN!)&v=\(API_VERSION)")
        queue.addOperation(getDataOperation)
        
        let parseData = ParseAndSaveData(parseType: .groupReorganize)
        parseData.addDependency(getDataOperation)
        queue.addOperation(parseData)
        
        let updateUI = UpdateUI(uiType: .alertAdd, targetVC: viewControllerNeedToUpdate, groupForActionsWGroups: group)
        updateUI.addDependency(parseData)
        OperationQueue.main.addOperation(updateUI)
    }
    
    func leaveGroupAndUpdateUI(group: GroupModel, viewControllerNeedToUpdate: UIViewController)
    {
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)groups.leave?group_id=\(group.id)&access_token=\(ACCESS_TOKEN!)&v=\(API_VERSION)")
        queue.addOperation(getDataOperation)
        
        let parseData = ParseAndSaveData(parseType: .groupReorganize)
        parseData.addDependency(getDataOperation)
        queue.addOperation(parseData)
        
        let updateUI = UpdateUI(uiType: .alertDelete, targetVC: viewControllerNeedToUpdate, groupForActionsWGroups: group)
        updateUI.addDependency(parseData)
        OperationQueue.main.addOperation(updateUI)
    }
    
    func getNewsFeed(startFrom: String = "", viewControllerNeedToUpdate: UIViewController)
    {
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)newsfeed.get?max_photos=1&start_from=\(startFrom)&filters=post&count=20&access_token=\(ACCESS_TOKEN!)&v=\(API_VERSION)")
        queue.addOperation(getDataOperation)
        
        let parseData = ParseAndSaveData(parseType: .newsFeed)
        parseData.addDependency(getDataOperation)
        queue.addOperation(parseData)
        
        let updateUI = UpdateUI(uiType: .tableView, targetVC: viewControllerNeedToUpdate)
        updateUI.addDependency(parseData)
        OperationQueue.main.addOperation(updateUI)
    }
    
    func getMessagesHistory()
    {
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)messages.get?count=200&time_offset=0&access_token=\(ACCESS_TOKEN!)&v=\(API_VERSION)")
        queue.addOperation(getDataOperation)
        
        let parseData = ParseAndSaveData(parseType: .getMessageHistory)
        parseData.addDependency(getDataOperation)
        queue.addOperation(parseData)
    }
    
//    func getLongPoolHistory()
//    {
//        guard let ts = RealmApi.getLPSessionData()?.first?.ts, let pts = RealmApi.getLPSessionData()?.first?.pts else
//        {
//            return
//        }
//        let queue = OperationQueue()
//        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)messages.getLongPollHistory?ts=\(ts)&pts=\(pts)&access_token=\(ACCESS_TOKEN!)&lp_version=\(LONG_POLL_VERSION)&v=\(API_VERSION)")
//        queue.addOperation(getDataOperation)
//
//        let parseData = ParseAndSaveData(parseType: .getMessageHistory)
//        parseData.addDependency(getDataOperation)
//        queue.addOperation(parseData)
//    }
    
    func getSessionDataForLongPoll()
    {
        guard let token = getToken() else
        {
            return
        }
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "\(BASE_URL)messages.getLongPollServer?need_pts=1&lp_version=\(LONG_POLL_VERSION)&access_token=\(token)&v=\(API_VERSION)")
        queue.addOperation(getDataOperation)
        
        let parseAndSaveOperation = ParseAndSaveData(parseType: .lpServerData)
        parseAndSaveOperation.addDependency(getDataOperation)
        queue.addOperation(parseAndSaveOperation)
    }
    
    func updateLPConnection()
    {
        guard let lpData = RealmApi.getLPSessionData()?.first else
        {
            return
        }
        let queue = OperationQueue()
        let getDataOperation = GetDataOperation(urlStr: "https://\(lpData.server ?? "")?act=a_check&key=\(String(lpData.key ?? ""))&ts=\(lpData.ts)&wait=25&mode=168&version=2")
        queue.addOperation(getDataOperation)
        
        let parseData = ParseAndSaveData(parseType: .updateLPData)
        parseData.addDependency(getDataOperation)
        queue.addOperation(parseData)
    }
}
